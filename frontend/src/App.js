import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/Home";
import Page404 from "./Pages/Page404";
import DetailPage from "./Pages/Detail";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<HomePage/>} path="/" />
        <Route element={<DetailPage/>} path="/details" />
        <Route path="*" element={<Page404 />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
