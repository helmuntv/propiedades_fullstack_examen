import React, { useEffect } from "react";
import { Container, ListGroup, ListGroupItem } from "react-bootstrap";
import NavigationBar from "../Components/Navbar";

import { useSelector } from 'react-redux';
import store from "../redux/store";
import { getPropertyDetails } from "../redux/actionCreators";
import { useLocation } from "react-router-dom";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import GoogleMapReact from 'google-map-react';
import { FaMapMarkerAlt } from "react-icons/fa";

const AnyReactComponent = ({ text }) => <div>{text}</div>;
const GOOGLE_KEY = process.env.REACT_APP_API_KEY_GOOGLE_MAPS

const props ={
  center: {
    lat: 19.4114791,
    lng: -99.1762838
  },
  zoom: 13
};

const DetailPage = () => {
    const { search } = useLocation();
    
    const property = 
    useSelector((state) => state.propertyReducer.property) 
    useEffect(() => {
        const propertyId = new URLSearchParams(search)
        store.dispatch(getPropertyDetails(propertyId.get("id")));
    });

    const onChange = () => console.log("change")
    const onClickItem = () => console.log("changeItem")
    const onClickThumb = () => console.log("changeThumb")
    

    return (
        <div>
          <NavigationBar />
          <Container>
            <div>
              <Container>
                {property ? (
                    <><Carousel showArrows={true} onChange={onChange} onClickItem={onClickItem} onClickThumb={onClickThumb}>
                        {property.property_image.map((image) => (
                            <div key={image.id}>
                                <img src={image.path} alt="Imagen propiedad"/>
                                <p className="legend">
                                  {property.property_type} para {property.operation} <br/>
                                  {property.street} {property.cp}<br/>
                                  {property.neighborhood}, {property.state}<br/>
                                  {property.price}
                                </p>
                            </div>
                        ))}
                    </Carousel>
                    <Container>
                      <div>
                        <h2><strong>Detalles del inmueble</strong></h2>
                        <p>{property.description}</p>
                      </div>
                      <div>
                        <h3><strong>Caracteristicas del inmueble</strong></h3>
                        <ListGroup>
                          <ListGroupItem>Dormitorios: {property.bedrooms}</ListGroupItem>
                          <ListGroupItem>Baños:       {property.num_bathrooms}</ListGroupItem>
                          <ListGroupItem>Metros Cuadrados: {property.m2_construction}</ListGroupItem>
                          <ListGroupItem>Estacionamientos: {property.parking}</ListGroupItem>
                          <ListGroupItem>Años: {property.age}</ListGroupItem>
                          <ListGroupItem>Departamentos: {property.departments}</ListGroupItem>
                          <ListGroupItem>Pisos: {property.floor}</ListGroupItem>
                        </ListGroup>
                        <h3><strong>Amenidades</strong></h3>
                        <ListGroup>
                          {property.property_amenity.map((amenity) => (
                            <ListGroupItem>
                              {amenity.amenity.name}
                            </ListGroupItem>      
                          ))}
                        </ListGroup>
                      </div>
                    </Container>
                    <br/>
                    <div style={{ height: '100vh', width: '100%' }}>
                        <GoogleMapReact
                        bootstrapURLKeys={{ key:`${GOOGLE_KEY}`}}    
                        defaultCenter={{lat:parseFloat(property.latitude),lng:parseFloat(property.longitude)}}
                        defaultZoom={props.zoom}
                        >
                        <AnyReactComponent
                            lat={parseFloat(property.latitude)}
                            lng={parseFloat(property.longitude)}
                            text={<FaMapMarkerAlt size={"lg"} style={{width: "16px"}}/>}
                        />
                        </GoogleMapReact>
                    </div>
                    </>
                ) : (
                 <div></div>
                )}
              </Container>
            </div>
          </Container>
        </div>
      );
}

export default DetailPage;