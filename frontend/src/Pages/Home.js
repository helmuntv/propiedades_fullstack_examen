import React, { useEffect } from "react";
import { Col, Row, Container } from "react-bootstrap";
import NavigationBar from "../Components/Navbar";

import { useSelector } from 'react-redux';
import store from "../redux/store";
import { getAllProperties } from "../redux/actionCreators";
import Card from 'react-bootstrap/Card'
import { FaTrash,FaEdit,FaEye } from "react-icons/fa";
import { Link } from "react-router-dom";

const HomePage = () => {
  const properties =
  useSelector((state) => state.propertyReducer.properties) 
  useEffect(() => {
    store.dispatch(getAllProperties());
  }, []);
  
  return (
    <div>
      <NavigationBar />
      <Container>
        <div>
          <Container>
            {properties ? (
              <Row md={3} sm={2} xs={1}>
                {properties.map((property) => (
                  <Col key={property.id}>
                    <Card style={{ width: '18rem' }}>
                      <Card.Img variant="top" src={property.property_image[0]['path']} />
                      <Card.Body style={{ display: "flex", justifyContent: "space-around"}}>
                        <Link to={`/details?id=${property.id}`}><FaEye style={{width: "16px"}}/></Link>
                        <FaEdit style={{width: "16px"}}/>
                        <FaTrash style={{width: "16px"}}/>
                      </Card.Body>
                    </Card>
                  </Col>
                ))}
              </Row>
            ) : (
             <div></div>
            )}
          </Container>
        </div>
      </Container>
    </div>
  );
};


export default HomePage;
