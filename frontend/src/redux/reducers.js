import {
  GET_ALL_PROPERTIES,
  GET_PROPERTY_DETAIL
} from "./actions";

export const propertyReducer = (state = {}, action) => {
  if (action.type === GET_ALL_PROPERTIES) {
    return {
      ...state,
      properties: action.properties,
    };
  }

  if (action.type === GET_PROPERTY_DETAIL) {
    return {
      ...state,
      property: action.property,
    };
  }
  return state
};
