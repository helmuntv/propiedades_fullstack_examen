<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'path',
        'order',
        'property'
    ];

    protected $hidden = [
        'image',
        'created_at',
        'updated_at'
    ];
    
    protected $table = 'property_images';

    #region relationships
    public function property(){
        return $this->belongsTo(Property::class);
    }

    public function image(){
        return $this->belongsTo(PropertyImage::class);
    }
    #end region relationship

    #region scopes
    #end region scopes
}
