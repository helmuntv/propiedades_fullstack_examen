<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $table = 'amenities';

    #region relationships
    public function propertyAmenities(){
        return $this->belongsToMany(PropertyAmenity::class);
    }
    #end region relationships
}
