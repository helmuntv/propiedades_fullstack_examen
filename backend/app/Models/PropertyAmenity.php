<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyAmenity extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'property_id',
        'amenity_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
    protected $table = 'property_amenities';

    #region relationships
    public function property(){
        return $this->belongsTo(Property::class);
    }

    public function amenity(){
        return $this->belongsTo(Amenity::class);
    }
    #end region relationship

    #region scopes
    public function scopeAmenity($query, $request){
        if($request->has('amenity_id')){
            $query->where('amenity_id',$request->amenity_id);
        }
    }
    #end region scopes
}
