<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'price',
        'property_type',
        'operation',
        'state',
        'city',
        'neighborhood',
        'cp',
        'street',
        'latitude',
        'longitude',
        'num_bathrooms',
        'bedrooms',
        'm2_construction',
        'parking',
        'age',
        'departments',
        'floor',
        'public_key',
        'user',
    ];

    protected $table = 'properties';

    #region relationship
    public function propertyAmenity(){
        return $this->hasMany(PropertyAmenity::class, 'property_id');
    }

    public function propertyImage(){
        return $this->hasMany(PropertyImage::class, 'property');
    }
    #end region relaitonship

    #region scopes
    public function scopeAmenity($query, $request){
        if($request->has('amenity_id')){
            $query->whereHas('propertyAmenity', function($query) use ($request){
                $query->amenity($request);
            });
        }
    }

    public function scopePrice($query, $request){
        if ($request->has('price')){
            $query->where('price', $request->price);
        }        
    }
    #end region scopes
}
