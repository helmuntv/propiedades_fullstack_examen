<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Property;
use App\Models\PropertyAmenity;
use App\Models\PropertyImage;

class PropertyController extends Controller
{
    public function index(){
        $propeties = Property::get();

        return $propeties;
    }
    
    /*public function mvcProperties()
    {
        $propeties = Property::get();
        return view('properties',compact('propeties'));
    }*/

    //> funcion para listar todas las propiedades
    //> GET /properties
    public function show(Request $request){
        Validator::make($request->all(),[
            'amenity_id' => 'sometimes|required|integer|exists:amenities,id',
            'price' => 'sometimes|required|integer'
        ])->validate();
        
        $properties = Property::amenity($request)
            ->price($request)
            ->with(['propertyImage' => function($query) {
                $query->with(['image']);
            }])->get();

        return $properties;
    }

    //> funcion para listar una propiedad por id
    //> GET /properties/{property_id}
    public function showOne(Request $request, $id){
        $request->request->add(['id' => $id]);

        Validator::make($request->all(),[
            'id' => 'required|integer|exists:properties,id'
        ])->validate();
        
        $property = Property::with([
            'propertyAmenity' => function($query) {
                $query->with(['amenity']);
            },
            'propertyImage' => function($query) {
                $query->with(['image']);
            }
        ])->findOrFail($id);
        
        return $property;
    }

    //> funcion para crear una propiedad
    //> POST /properties
    public function create(Request $request){
        Validator::make($request->all(), $this->rulesCreate($request))->validate();
        
        $property = Property::create($request->all());
        
        if ($request->has('images')){  
            foreach($request->images as $img){
                $image = new Request([
                    'path' => $img['path'],
                    'order' => $img['order'],
                    'property' => $property->id
                ]);
                PropertyImage::create($image->all());
            }
        }

        if ($request->has('amenities')){
            foreach($request->amenities as $amenity){
                $amty = new Request([
                    'amenity_id' => $amenity['amenity_id'],
                    'property_id' => $property->id
                ]);
                PropertyAmenity::create($amty->all());
            }
        }       

        return $property;
    }

    //> funcion para validar el request de creación de propiedades
    public function rulesCreate($request){
        return [
            'name' => 'required|string|min:10|max:300',
            'description' => 'required|string|min:20|max:300',
            'price' => 'required|numeric|between:1,9999999999.99',
            'property_type' => 'required|string|in:HOUSE,APARTAMENT,TERRAIN,OFFICE,LOCAL',
            'operation' => 'required|string|in:SALE,RENT,TRANSFER',
            'state' => 'required|string|min:3|max:100',
            'city' => 'required|string|min:3|max:150',
            'neighborhood' => 'required|string|min:3|max:100',
            'cp' => 'required|string|min:1|max:20',
            'street' => 'required|string|min:3|max:200',
            'latitude' => 'required|numeric|max:20',
            'longitude' => 'required|numeric|max:20',
            'num_bathrooms' => 'required|integer|min:1',
            'bedrooms' => 'required|integer|min:1',
            'm2_construction' => 'required|integer|min:1',
            'parking' => 'sometimes|required|integer|min:1',
            'age' => 'required|integer|min:1',
            'departments' => 'required|integer',
            'floor' => 'required|integer',
            'public_key' => 'required|string|min:1|max:100',
            'user' => 'required|integer|exists:users,id',
            'images' => 'required',
            'amenities' => 'sometimes|required'
        ];
    }
}
