<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Amenity;

class AmenityController extends Controller
{
    //>funcion para listar los tipos de amenidades
    //> GET /amenities
    public function show(){
        $amenities = Amenity::get();

        return $amenities;
    }
}
