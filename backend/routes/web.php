<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/home', [App\Http\Controllers\PropertyController::class, 'mvcProperties'])->name('properties');
Route::group(['middleware' => ['cors']], function(){
    Route::get('/', function () {
        return redirect()->route('properties');
    });
    
    //Route::get('/home', [App\Http\Controllers\PropertyController::class, 'show'])->name('properties');    
    //Property
    Route::get('/properties', [App\Http\Controllers\PropertyController::class, 'show'])->name('properties');
    Route::post('/properties', [App\Http\Controllers\PropertyController::class, 'create']);
    Route::get('/properties/{property}', [App\Http\Controllers\PropertyController::class, 'showOne']);


    //Amenity
    Route::get('amenities', [App\Http\Controllers\AmenityController::class, 'show']);
});
